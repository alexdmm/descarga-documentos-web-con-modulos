from robot import Robot

class Cache:
    def _init_(self):
        self.documents = {}

    def retrieve(self, url):
        if url not in self.documents:
            robot = Robot(url)
            robot.retrieve()
            self.documents[url] = robot.content()

    def show(self, url):
        self.retrieve(url)
        print(self.documents[url])

    def show_all(self):
        for url in self.documents:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.documents[url]
