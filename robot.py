import urllib.request


class Robot:
    def _init_(self, url):
        self.url = url
        self.downloaded = False
        self.document = ""

    def retrieve(self):
        if not self.downloaded:
            print("Descargando", self.url)
            response = urllib.request.urlopen(self.url)
            self.document = response.read().decode()
            self.downloaded = True

    def show(self):
        self.retrieve()
        print(self.document)

    def content(self):
        self.retrieve()
        return self.document
