#!/usr/bin/python3

from robot import Robot
from cache import Cache


# Programa principal
if __name__ == "_main_":
    # Crear objetos Robot
    robot1 = Robot("https://cursosweb.github.io/programas/IT-ST.pdf")
    robot2 = Robot("https://gsyc.urjc.es//")

    # Llamar a métodos de Robot
    robot1.retrieve()
    robot1.show()
    content = robot1.content()
    print(content)

    # Crear objetos Cache
    cache1 = Cache()
    cache2 = Cache()

    # Llamar a métodos de Cache
    cache1.retrieve("https://cursosweb.github.io/programas/IT-ST.pdf")
    cache1.show("https://cursosweb.github.io/programas/IT-ST.pdf")
    cache2.retrieve("https://gsyc.urjc.es//")
    cache2.show("https://gsyc.urjc.es//")
    cache2.show_all()
    content = cache2.content("https://cursosweb.github.io/programas/IT-ST.pdf")
    print(content)
